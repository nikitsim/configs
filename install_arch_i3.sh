#!/bin/sh

#Install some necessae programs
pacman -Suy
pacman -S sudo zsh zsh-completions i3wm i3lock adapta-gtk-theme powerline-fonts gnome-terminal firefox traceroute i3bar i3status xorg-xinit gpg ccid vim nitrokey-app imagemagick telegram-desktop compton wget git galculator scrot feh lightdm dhcpcd lightdm-gtk-greeter pulseaudio-alsa pulseaudio-bluetooth bluez-utils blueman

#Install oh-my-zsh
sh -c "$(wget -O- https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

#Set local time
ln -sf /usr/share/zoneinfo/Europe/Moscow /etc/localtime
hwclock --systohc

#Network configuration
echo "ArchDesktopi3" >> /etc/hostname
cat << EOF >> /etc/hosts
127.0.0.1	localhost
::1		localhost
127.0.1.1	ArchDesktopi3.localdomain	ArchDesktopi3
EOF

#Localization
rm /etc/locale.gen
cat << EOF >> /etc/locale.gen
en_US.UTF-8 UTF-8
ru_RU.UTF-8 UTF-8
EOF
locale-gen
echo LANG=en_US.UTF-8 >> /etc/locale.conf

#Setup LightDM
mkdir /usr/share/xgreeters
cp ./lightdm-gtk-greeter.desktop /usr/share/xgreeters/
systemctl set-default graphical.target
systemctl enable lightdm

#Add new user
useradd -m -g users -G scanner,power,video,storage,optical,lp,audio,wheel -s /bin/zsh nikita

#Add configs
mkdir -rf /home/nikita/.config/i3
cp i3/config /home/nikita/.config/i3
mkdir -rf /home/nikita/.config/rofi
cp rofi/config /home/nikita/.config/rofi
cp Xresources /home/nikita/.Xresources

